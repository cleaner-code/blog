---
layout: post
title:  "The Future of Serverless and Functions as a Service"
---

Amazon calls serverless Lambda but whatever you call it, astute developers will recognize it for what it is; another layer of abstraction.  Standing on the shoulders of giants is the way our industry has been able to accelerate the pace of advancement.  Each layer allows developers to ignore more and more *plumbing* and concentrate on the task at hand.  The latest layer of abstraction allows developers to simply write code push to the cloud.  

## So what's next?

With the proliferation of microservices and continued standardization of RPC protocols, I believe there will be a resurgence of business rules engines (BRE) which leverage microservices running on top of cloud function services.  BREs opens the door to subject matter experts (SMEs) to directly express the needs of the business / organization.

A move to BREs follows several industry trends:

- Agile Development - regular customer involvement is encouraged or required by agile development, BREs give the customer even more involvement
- Devops - Similar to how the role of development is becoming more operations focused to eliminate communication breakdowns; BREs reduce the chances of a communication breakdown between SMEs and developers
- History repeats itself - VMs, containers, smart phones, tablets...all attempted and failed in the past until the implementation improved

## I see a problem

Rules engines are notorious for growing organically and rapidly complex because the individuals writing rules are not concerned enough with maintainability.  This is where developers need to hook rules to microservices with narrow contexts and limit implicit behaviour.  The same rigor required to create maintainable microservices flow into a good BRE system which makes them a natural fit

What do you think?
